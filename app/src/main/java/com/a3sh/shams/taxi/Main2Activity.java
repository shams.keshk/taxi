package com.a3sh.shams.taxi;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInApi;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class Main2Activity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private FirebaseUser currentUser ;
    private GoogleApiClient mGoogleApiClient;
    FacebookAuthProvider facebookAuthProvider;


    @Override
    protected void onStart() {

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        mGoogleApiClient.connect();

        super.onStart();

    }

    private FirebaseDatabase database;
    private DatabaseReference myRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();

        //database = FirebaseDatabase.getInstance();
        myRef = FirebaseDatabase.getInstance().getReference();



        Button button2 = findViewById(R.id.add_data);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseUser user = mAuth.getCurrentUser();
                String userName = user.getDisplayName();
                String userEmail = user.getEmail();
                User userSignedUp = new User();
                userSignedUp.setEmail(userEmail);
                userSignedUp.setUserName(userName);
                userSignedUp.setPhotoUrl(Uri.parse(""+user.getPhotoUrl()).buildUpon()
                        .appendQueryParameter("type","large")
                        .build().toString());
                myRef.child("users").child(user.getUid()).setValue(userSignedUp);
            }
        });


        TextView tv = findViewById(R.id.tv);
        Uri builder = Uri.parse(""+currentUser.getPhotoUrl()).buildUpon()
                .appendQueryParameter("type","large")
                .build();
        tv.setText("Hi " + builder );


        ActionBar actionBar = getSupportActionBar();
        actionBar.setCustomView(R.layout.custom_profile_image_view);
        ImageView profileImage = actionBar.getCustomView().findViewById(R.id.profile_image_home_activity);

        /*ActionBar.LayoutParams layoutParams = new ActionBar.LayoutParams(
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.WRAP_CONTENT, Gravity.RIGHT
               | Gravity.CENTER_VERTICAL);
        layoutParams.rightMargin = 16;
        */
      //  profileImage.setLayoutParams(layoutParams);

        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM
                  | ActionBar.DISPLAY_SHOW_HOME);

        Glide.with(this).load(builder).into(profileImage);

        TextView userNameTextView = actionBar.
                getCustomView().
                findViewById(R.id.user_name_text_view);

        userNameTextView.setText(currentUser.getDisplayName());

        Button button = findViewById(R.id.log_out);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                Auth.GoogleSignInApi.signOut(mGoogleApiClient);
                Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient);
                finish();
            }
        });




    }
}
