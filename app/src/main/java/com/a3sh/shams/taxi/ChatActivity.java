package com.a3sh.shams.taxi;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.a3sh.shams.taxi.adapter.ChatMessagesAdapter;
import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChatActivity extends AppCompatActivity implements ChatMessagesAdapter.MessagesListClickListener{

    FloatingActionButton sendMessageFloatingActionButton;

    @BindView(R.id.included_message_area_layout_id)
    LinearLayout viewIncluded;
    @BindView(R.id.edit_text_message_input)
    EditText messageEditText ;

    @BindView(R.id.rv_chat_messages_recycler_view)
    RecyclerView messagesRecyclerView;

    User user;
    private FirebaseAuth mAuth;
    private FirebaseUser currentUser ;
    private List<ChatMessage> messagesArrayList;

    private FirebaseListAdapter<ChatMessage> chatMessageFirebaseListAdapter;
    private FirebaseRecyclerAdapter<ChatMessage,ChatMessagesAdapter.AdapterViewHolder>
            firebaseRecyclerAdapter;


    private ChatMessagesAdapter chatMessagesAdapter;

    @Override
    public void onMessagesListClickListener(int position) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        ButterKnife.bind(this);

        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();

        sendMessageFloatingActionButton =
                viewIncluded.findViewById(R.id.btn_send_message_image_btn);

        user = new User();

        messagesArrayList = new ArrayList<>();


        if (getIntent().hasExtra("USER_OBJECT_KEY")){
            user = getIntent().getParcelableExtra("USER_OBJECT_KEY");
        }

        chatMessagesAdapter = new ChatMessagesAdapter(listMessages(),this,this,currentUser);
       // chatMessagesAdapter = new ChatMessagesAdapter(messagesArrayList,this,this,currentUser);


        Uri builder = Uri.parse("" + user.getPhotoUrl()).buildUpon()
                .appendQueryParameter("type","large")
                .build();
        Toast.makeText(this,builder.toString()+"",Toast.LENGTH_LONG).show();

        ActionBar actionBar = getSupportActionBar();
        actionBar.setCustomView(R.layout.custom_profile_image_view);
        ImageView profileImage = actionBar.getCustomView().findViewById(R.id.profile_image_home_activity);

        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM
                | ActionBar.DISPLAY_SHOW_HOME);

        Glide.with(this).load(builder).into(profileImage);

        final TextView userNameTextView = actionBar.
                getCustomView().
                findViewById(R.id.user_name_text_view);

         userNameTextView.setText(user.getUserName());

         messagesRecyclerView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
             @Override
             public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                 if ( bottom < oldBottom) {
                     messagesRecyclerView.postDelayed(new Runnable() {
                         @Override
                         public void run() {
                            messagesRecyclerView.scrollToPosition(messagesArrayList.size() - 1);
                          //   messagesRecyclerView.smoothScrollToPosition(messagesArrayList.size() - 1);
                         }
                     }, 5);
                 }
             }
         });

         sendMessageFloatingActionButton.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {

             String message = messageEditText.getText().toString();

             FirebaseDatabase.getInstance().getReference()
                     .child("messages").
                     child(user.getUserId())
                     .child(currentUser.getUid()).push()
                     .setValue(new ChatMessage(message,currentUser.getEmail(),currentUser.getUid()));
             FirebaseDatabase.getInstance().getReference()
                     .child("messages").
                     child(currentUser.getUid())
                     .child(user.getUserId()).push()
                     .setValue(new ChatMessage(message,user.getEmail(),currentUser.getUid()));
                    // .push().setValue(new ChatMessage(message,currentUser.getEmail()));
            /// myRef.child("users").child(user.getUid()).setValue(userSignedUp);
             messageEditText.setText("");

         }
     });

        LinearLayoutManager linearLayoutManager =
                new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        messagesRecyclerView.setLayoutManager(linearLayoutManager);
        messagesRecyclerView.setHasFixedSize(true);

      messagesRecyclerView.setAdapter(chatMessagesAdapter);


      //  displayChatMessages();
    }

    private void hideUserDisplayMyText(TextView textView,TextView userTextView){
        textView.setVisibility(View.VISIBLE);
        userTextView.setVisibility(View.GONE);
    }
    private void hideMyTextDisplayUserText(TextView textView,TextView myTextView){
        textView.setVisibility(View.VISIBLE);
        myTextView.setVisibility(View.GONE);
    }
    private void displayChatMessages(){

        firebaseRecyclerAdapter =
                new FirebaseRecyclerAdapter<ChatMessage, ChatMessagesAdapter.AdapterViewHolder>(ChatMessage.class ,
                        R.layout.chat_messages_list_item,
                        ChatMessagesAdapter.AdapterViewHolder.class,FirebaseDatabase.getInstance().getReference()
                        .child("messages").
                                child(user.getUserId())
                        .child(currentUser.getUid())) {
                    @Override
                    protected void populateViewHolder(ChatMessagesAdapter.AdapterViewHolder viewHolder
                            , ChatMessage model, int position) {

                        String message = model.getMessageText();
                        String userEmail = model.getMessageUser();
                        Toast.makeText(getApplicationContext(),message + " / "+ userEmail,
                                Toast.LENGTH_LONG).show();
                        if (userEmail.equals(currentUser.getEmail())){
                            viewHolder.myMessage.setText(message);
                            hideUserDisplayMyText(viewHolder.myMessage,viewHolder.userMessage);
                        }else {
                            viewHolder.userMessage.setText(message);
                            hideMyTextDisplayUserText(viewHolder.userMessage,viewHolder.myMessage);
                        }
                    }
                };

        /*
        chatMessageFirebaseListAdapter =
                new FirebaseListAdapter<ChatMessage>(this,ChatMessage.class,
                        R.layout.chat_messages_list_item,
                        FirebaseDatabase.getInstance().getReference()
                                .child("messages").
                                child(user.getUserId())
                                .child(currentUser.getUid())){
                    @Override
                    protected void populateView(View v, ChatMessage model, int position) {
                        TextView userMessage = v.findViewById(R.id.tv_user_that_sent_to_me_message_text_view);
                        TextView myMessage = v.findViewById(R.id.tv_my_sent_message_text_view);

                        String message = model.getMessageText();
                        String userEmail = model.getMessageUser();
                        Toast.makeText(getApplicationContext(),message + " / "+ userEmail,
                                Toast.LENGTH_LONG).show();
                       if (userEmail.equals(currentUser.getEmail())){
                            myMessage.setText(message);
                            hideUserDisplayMyText(myMessage,userMessage);
                        }else {
                            userMessage.setText(message);
                            hideMyTextDisplayUserText(userMessage,myMessage);
                        }

                    }
                };
        */
      messagesRecyclerView.setAdapter(firebaseRecyclerAdapter);


    }

   public List<ChatMessage> listMessages() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
     /*   DatabaseReference myRef = database.getReference().child("messages")
                . child(user.getUserId())
                .child(currentUser.getUid());
                */
        DatabaseReference secondDatabaseReference =
                database.getReference().child("messages")
                .child(currentUser.getUid())
                .child(user.getUserId());

     /*   myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                messagesArrayList.clear();
                for(DataSnapshot ds : dataSnapshot.getChildren()) {
                    String messageText = ds.child("messageText").getValue(String.class);
                    String messageUserEmail = ds.child("messageUser").getValue(String.class);

                    //Log.e(TAG, userName + " / " + photoUrl + " kkkkkkkkkkkkkkkk");
                    ChatMessage message = new ChatMessage();
                    message.setMessageText(messageText);
                    message.setMessageUser(messageUserEmail);
                    // activityAdapter.clearAdapter();
                    messagesArrayList.add(message);

                }

                chatMessagesAdapter.notifyDataSetChanged();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });

*/
        secondDatabaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                messagesArrayList.clear();

                for(DataSnapshot ds : dataSnapshot.getChildren()) {
                    String messageText = ds.child("messageText").getValue(String.class);
                    String messageUserEmail = ds.child("messageUser").getValue(String.class);
                    String messageUserId = ds.child("messageUserId").getValue(String.class);

                    //Log.e(TAG, userName + " / " + photoUrl + " kkkkkkkkkkkkkkkk");
                    ChatMessage message = new ChatMessage();
                    message.setMessageText(messageText);
                    message.setMessageUser(messageUserEmail);
                    message.setMessageUserId(messageUserId);
                    // activityAdapter.clearAdapter();
                    messagesArrayList.add(message);

                }

                messagesRecyclerView.scrollToPosition(messagesArrayList.size() - 1);
                chatMessagesAdapter.notifyDataSetChanged();
              // mAdapter.notifyItemInserted(mChats.size() - 1);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        return messagesArrayList;
    }

}
