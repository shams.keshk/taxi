package com.a3sh.shams.taxi;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.a3sh.shams.taxi.adapter.HomeActivityAdapter;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends AppCompatActivity implements
        HomeActivityAdapter.UserListClickListener{

    private FirebaseAuth mAuth;
    private FirebaseUser currentUser ;
    private GoogleApiClient mGoogleApiClient;
    FacebookAuthProvider facebookAuthProvider;


    private FirebaseDatabase database;
    private DatabaseReference myRef;
    private HomeActivityAdapter activityAdapter;
    private List<User> userArrayList;
    private static final String TAG = HomeActivity.class.getSimpleName();

    @BindView(R.id.rv_home_activity)
    RecyclerView recyclerView;

    @Override
    protected void onStart() {

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        mGoogleApiClient.connect();

        super.onStart();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();


        userArrayList = new ArrayList<>();

        activityAdapter = new
                HomeActivityAdapter(listUsers(),this);

        LinearLayoutManager gridLayoutManager =
                new LinearLayoutManager(this);
        gridLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setHasFixedSize(true);

        recyclerView.setAdapter(activityAdapter);

      //  displayList();

        Button button = findViewById(R.id.log_out_home_activity);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                Auth.GoogleSignInApi.signOut(mGoogleApiClient);
                Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient);
                finish();
            }
        });


    }

    @Override
    public void onUserListClickListener(int position) {
        User user = activityAdapter.getItem(position);
        Intent intent = new Intent(HomeActivity.this, ChatActivity.class);
        intent.putExtra("USER_OBJECT_KEY", user);
        startActivity(intent);
    }

    public List<User> listUsers() {
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference().child("users/");

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

             for(DataSnapshot ds : dataSnapshot.getChildren()) {
                    String userName = ds.child("userName").getValue(String.class);
                    String photoUrl = ds.child("photoUrl").getValue(String.class);
                    String userEmail = ds.child("email").getValue(String.class);
                    String userId = ds.child("userId").getValue(String.class);

                    Log.e(TAG, userName + " / " + photoUrl + " kkkkkkkkkkkkkkkk");
                 User user = new User();
                    user.setUserName(userName);
                    user.setPhotoUrl(photoUrl);
                    user.setEmail(userEmail);
                    user.setUserId(userId);
                // activityAdapter.clearAdapter();
                 userArrayList.add(user);

                }
            //    String userId = dataSnapshot.getValue(String.class);
               // User userMap = dataSnapshot.getValue(User.class);
               // Map<String, Object> map = (Map<String, Object>) dataSnapshot.getValue();
             //   Log.e(TAG,map +"jjjjjjjjjjjj");

              //  System.out.println("userMap");
               // activityAdapter.setmUsersList(userArrayList);
             activityAdapter.notifyDataSetChanged();



            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });
        return userArrayList;
    }
}
