package com.a3sh.shams.taxi;

import java.util.Date;

public class ChatMessage  {

    private String messageText;
    private String messageUser;
    private long messageDate;
    private String messageUserId;

    public ChatMessage(){}

    public ChatMessage(String messageText, String messageUser) {
        this.messageText = messageText;
        this.messageUser = messageUser;
        this.messageDate = new Date().getTime();
    }

    public ChatMessage(String messageText, String messageUser , String messageUserId) {
        this.messageText = messageText;
        this.messageUser = messageUser;
        this.messageDate = new Date().getTime();
        this.messageUserId = messageUserId;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public String getMessageUser() {
        return messageUser;
    }

    public void setMessageUser(String messageUser) {
        this.messageUser = messageUser;
    }

    public long getMessageDate() {
        return messageDate;
    }

    public void setMessageDate(long messageDate) {
        this.messageDate = messageDate;
    }

    public String getMessageUserId() {
        return messageUserId;
    }

    public void setMessageUserId(String messageUserId) {
        this.messageUserId = messageUserId;
    }
}
