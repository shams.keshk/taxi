package com.a3sh.shams.taxi.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.a3sh.shams.taxi.ChatMessage;
import com.a3sh.shams.taxi.R;
import com.a3sh.shams.taxi.User;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChatMessagesAdapter extends RecyclerView.Adapter<ChatMessagesAdapter. AdapterViewHolder>  {

    private List<ChatMessage> mMessagesList;
    private MessagesListClickListener messagesListClickListener;
    private Context context;
    private FirebaseUser currentUser;
    private static final String TAG = ChatMessagesAdapter.class.getSimpleName();


    public ChatMessagesAdapter(List<ChatMessage> chatMessageList,
                               MessagesListClickListener messagesListClickListener , Context  context , FirebaseUser currentUser) {
        mMessagesList = chatMessageList;
        this.messagesListClickListener = messagesListClickListener;
        this.context = context;
        this.currentUser = currentUser;
    }

    public List<ChatMessage> getmMessagesList() {
        return mMessagesList;
    }

    public void setmMessagesList(List<ChatMessage> mMessagesList) {
        this.mMessagesList = mMessagesList;
        notifyDataSetChanged();
    }

    public void clearAdapter() {
        int size = mMessagesList.size();
        mMessagesList.clear();
        notifyItemRangeRemoved(0, size);
    }

    public ChatMessage getItem(int position) {
        return mMessagesList.get(position);
    }

    @NonNull
    @Override
    public ChatMessagesAdapter.AdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        int layoutResourceOfListItemId = R.layout.chat_messages_list_item;
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(layoutResourceOfListItemId, parent, false);

        return new ChatMessagesAdapter.AdapterViewHolder(view);
    }

    private void hideUserDisplayMyText(TextView myLinearLayout,TextView userLinearLayout){
        myLinearLayout.setVisibility(View.VISIBLE);
        userLinearLayout.setVisibility(View.GONE);
    }
    private void hideMyTextDisplayUserText(TextView userLinearLayout , TextView myLinearLayout){
        userLinearLayout.setVisibility(View.VISIBLE);
        myLinearLayout.setVisibility(View.GONE);
    }

    @Override
    public void onBindViewHolder(@NonNull ChatMessagesAdapter.AdapterViewHolder holder, int position) {

        ChatMessage user = getItem(position);



        String message = user.getMessageText();
        String userEmail = user.getMessageUser();
        Toast.makeText(context.getApplicationContext(),message + " / "+ userEmail,
                Toast.LENGTH_LONG).show();
        if (user.getMessageUserId().equals(currentUser.getUid())){
            holder.myMessage.setVisibility(View.VISIBLE);
            holder.userMessage.setVisibility(View.GONE);
            holder.myMessage.setText(message);
            Log.e(TAG, "onBindViewHolder: User Id : " + user.getMessageUserId() +
            " ,, Current User Id Is : " + currentUser.getUid() + "Are Equal ");
          // hideUserDisplayMyText(holder.myMessage,holder.userMessage);
        }else {
            holder.myMessage.setVisibility(View.GONE);
            holder.userMessage.setVisibility(View.VISIBLE);
            holder.userMessage.setText(message);
            Log.e(TAG, "onBindViewHolder: User Id : " + user.getMessageUserId() +
                    " ,, Current User Id Is : " + currentUser.getUid() + "Are Not Equal ");
            // hideMyTextDisplayUserText(holder.userMessage,holder.myMessage);
        }

        /*if (user.getPhotoUrl() != null) {
            Uri uri = Uri.parse(user.getPhotoUrl()).buildUpon()
                    .build();

            Picasso.get().load(uri).into(holder.mUserPhototImageView);
        }

        if (user.getUserName() != null) {
            holder.mUserNameTextView.setText(user.getUserName());
        }
        */
    }

    @Override
    public int getItemCount() {
        if (mMessagesList.size() > 0) {
            return mMessagesList.size();
        }
        return 0;
    }

    public interface MessagesListClickListener {
        void onMessagesListClickListener(int position);
    }


    public class AdapterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

       // @BindView(R.id.tv_user_that_sent_to_me_message_text_view)
        //TextView userMessage;
       // @BindView(R.id.tv_my_sent_message_text_view)
       public TextView myMessage;
       public TextView userMessage ;


        public AdapterViewHolder(View view) {
            super(view);
            //ButterKnife.bind(this, view);
            myMessage = view.findViewById(R.id.tv_user_that_sent_to_me_message_text_view);
            userMessage = view.findViewById(R.id.tv_my_sent_message_text_view);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            messagesListClickListener.onMessagesListClickListener(getAdapterPosition());
        }
    }

}

