package com.a3sh.shams.taxi.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.a3sh.shams.taxi.R;
import com.a3sh.shams.taxi.User;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivityAdapter extends RecyclerView.Adapter<HomeActivityAdapter.AdapterViewHolder>  {

    private List<User> mUserList;
    private UserListClickListener userListClickListener;


    public HomeActivityAdapter(List<User> userArrayList, UserListClickListener userListClickListener) {
        mUserList = userArrayList;
        this.userListClickListener = userListClickListener;
    }

    public List<User> getmUsersList() {
        return mUserList;
    }

    public void setmUsersList(List<User> mMoviesList) {
        this.mUserList = mMoviesList;
        notifyDataSetChanged();
    }

    public void clearAdapter() {
        int size = mUserList.size();
        mUserList.clear();
        notifyItemRangeRemoved(0, size);
    }

    public User getItem(int position) {
        return mUserList.get(position);
    }

    @NonNull
    @Override
    public AdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        int layoutResourceOfListItemId = R.layout.list_of_profiles_item;
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(layoutResourceOfListItemId, parent, false);

        return new AdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterViewHolder holder, int position) {
        User user = getItem(position);

        if (user.getPhotoUrl() != null) {
            Uri uri = Uri.parse(user.getPhotoUrl()).buildUpon()
                    .build();

            Picasso.get().load(uri).into(holder.mUserPhototImageView);
        }

        if (user.getUserName() != null) {
            holder.mUserNameTextView.setText(user.getUserName());
        }
    }

    @Override
    public int getItemCount() {
        if (mUserList.size() > 0) {
            return mUserList.size();
        }
        return 0;
    }

    public interface UserListClickListener {
        void onUserListClickListener(int position);
    }


    public class AdapterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        @BindView(R.id.imgv_user_profile_home_activity)
        ImageView mUserPhototImageView;
        @BindView(R.id.tv_user_name_home_activity)
        TextView mUserNameTextView;

        public AdapterViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            userListClickListener.onUserListClickListener(getAdapterPosition());
        }
    }

}
